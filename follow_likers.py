from instapy import (InstaPy, smart_run)

session = InstaPy(username='username', password='password')

with smart_run(session):
    session.set_do_follow(enabled=True, percentage=100)
    session.set_do_like(enabled=True, percentage=100)
    session.follow_likers(['pedroovfonseca'],
                          photos_grab_amount=2, follow_likers_per_photo=2, randomize=True, sleep_delay=600, interact=True)

    comments = ['Nice shot!', 'Love your post!']
    session.set_do_comment(enabled=True, percentage=100)
    session.set_comments(comments, media='Photo')
    session.join_pods()
