from instapy import (InstaPy, smart_run)


session = InstaPy(username='username', password='password')

with smart_run(session):
    session.set_do_follow(enabled=True, percentage=90)  # Seguir pessoas
    session.set_do_like(enabled=True, percentage=100)  # Curtir postagens
    session.like_by_tags(['players', 'photos', 'blessed'], amount=5
                         )  # Curtir postagens com hashtag ...

    # Comentários que o bot irá fazer
    comments = ['Nice shot!', 'Cool your post']
    session.set_do_comment(enabled=True, percentage=95)
    session.set_comments(comments, media='Photo')
    session.join_pods()
