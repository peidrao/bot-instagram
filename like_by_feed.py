from instapy import (InstaPy, smart_run)


session = InstaPy(username='username', password='password')


with smart_run(session):
    session.set_do_follow(enabled=True, percentage=100)
    session.set_do_like(enabled=True, percentage=100)

    session.like_by_feed(amount=3, randomize=True,
                         unfollow=False, interact=True)

    comments = ['Nice shot!', 'Love your post!']
    session.set_do_comment(enabled=True, percentage=80)
    session.set_comments(comments, media='Photo')
    session.join_pods()
