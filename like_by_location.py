from instapy import (InstaPy, smart_run)

sesion = InstaPy(username='username', password='password')

with smart_run(sesion):
    sesion.set_do_follow(enabled=True, percentage=100)
    sesion.set_do_like(enabled=True, percentage=100)

    sesion.like_by_locations(['213119086/natal-rio-grande-do-norte'], amount=3)

    comments = ['Nice shot', 'Love your posts!']
    sesion.set_do_comment(enabled=True, percentage=80)
    sesion.set_comments(comments, media='Photo')
    sesion.join_pods()
