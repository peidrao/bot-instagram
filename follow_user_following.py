from instapy import (InstaPy, smart_run)

session = InstaPy(username='username', password='password')

with smart_run(session):
    session.follow_user_following(['pedroovfonseca'], amount=3, randomize=True)
    session.join_pods()
