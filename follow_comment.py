from instapy import (InstaPy, smart_run)

session = InstaPy(username='username', password='password')

with smart_run(session):
    session.set_do_follow(enabled=True, percentage=100)
    session.set_do_like(enabled=True, percentage=100)
    session.follow_commenters(
        ['pedroovfonseca'], amount=4, daysold=30, max_pic=100)

    comments = ['Nice shot!', 'Love your post!']
    session.set_do_comment(enabled=True, percentage=100)
    session.set_comments(comments, media='Photo')
    session.join_pods()
