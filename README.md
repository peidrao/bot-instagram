# InstaPy

Para rodar o arquivo precisa-se colocar o usuário e senha do instagram

Após isso, basta rodar: 

> python script.py

## Alguns métodos
- **follow_user_followers**: seguir pessoas que o meu seguidor está seguindo.
- **like_by_feed**: like em postagens que estao no feed
- **follow_user_followers**: seguir seguidores de um determinado perfil
- **Follow_likers**: seguir pessoas que curtiram uma foto de uma pessoa que estou seguindo
- **follow_commenters** : seguir pessoas que comentam uma foto de uma pessoa que estou seguindo


##  like_by_locations (Ccurtir, seguir e fazer comentários que estão em uma determinada localização.) 


```pyhton3
sesion.like_by_locations(['213119086 natal-rio-grande-do-norte'], amount=3) 
```

Dentro da lista, podem haver várias localizações. 


## Unfollow_users - allFollowing (Deixar de seguir pessoas)
- allFollowing = Deixar de seguir qualquer pessoa da nossa lista
- style = Modo na qual deixaremos de seguir as pessoas (existem 3 modos)
  - **Fifo** = Primeiro a entrar é o primeiro a sair (fila
  - **Lifo** = Último a entrar é o primeiro a sair (pilha)
  - **random** = de modo randômico.
- unfollow_after= 3 * 60 * 60 = Tempo que será determinado para exclusão (deixarei de segui apenas usuários em que fazem mais de 3 horas que sigo) 


 ## Unfollow_users - nonFollowers (deixar de seguir pessoas que não nos seguem)
    allFollowing = Deixar de seguir qualquer pessoa da nossa lista




## Entendedo o  "percentage"

O percentagem significa a probabilidade em que o bot vai fazer as ações que delimitamos
Quanto maior a porcentagem, maior será a chance do bot fazer as ações.


## Variáveis de parâmetros
- **amount** = Quantidade de postagens que serão curtidas
- **randomize** = Fará a randomização das postagens que serão curtidas (Terão postagens que serão curtidas e outras não)
- **unfollow** = Deixará de seguir os usuários (caso seja true)
- **interact** = Poderá fazer um comentário da postagem
- **photos_grab_amount** = Quantas fotos o robô vai selecionar para seguir as pessoas
- **follow_likers_per_photo** = Quantas pessoas serão seguidas por foto
- **sleep_delay** = Colocar o programa em espera (fazer a simulação de que o robô é um usuário comum)
- **daysold** = Não vai pegar comentários antigos (100 = 100 dias)
- **max_pic** = Limitar o número de fotos a serem analizadas
- **sleep_delay** = Colocar o programa em espera (fazer a simulação de que o robô é um usuário comum)
    """