from instapy import (InstaPy, smart_run)

user = InstaPy(username='username', password='password')

with smart_run(user):
    user.unfollow_users(amount=2, nonFollowers=True,
                        style='FIFO', unfollow_after=3*60*60, sleep_delay=450)
